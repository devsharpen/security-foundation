<?php

declare(strict_types = 1);

namespace Devsharpen\Security\Foundation\Core\Exception;

class AuthorizationException extends \RuntimeException
{
}