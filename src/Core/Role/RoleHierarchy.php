<?php

declare(strict_types = 1);

namespace Devsharpen\Security\Foundation\Core\Role;

interface RoleHierarchy
{
    public function availableRoles(array $roles): array;
}