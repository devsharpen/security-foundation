<?php

declare(strict_types = 1);

namespace Devsharpen\Security\Foundation\Core\Role;

interface Role
{
    public function getRole() : string; // name

    public function __toString(): string;
}