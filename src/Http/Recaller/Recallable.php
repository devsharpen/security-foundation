<?php

declare(strict_types = 1);

namespace Security\Foundation\Http\Recaller;

use Devsharpen\Security\Foundation\Core\Authentication\Token\Tokenable;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

interface Recallable
{
    public const COOKIE_NAME = '_security_remember_me_cookie';

    public function autoLogin(Request $request);

    public function loginFail(Request $request);

    public function loginSuccess(Request $request, Response $response, Tokenable $token);
}