<?php

declare(strict_types = 1);

namespace Security\Foundation\Http\EntryPoint;

use Devsharpen\Security\Foundation\Core\Exception\AuthenticationException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

interface EntryPoint
{
    public function start(Request $request, AuthenticationException $exception = null): Response;
}