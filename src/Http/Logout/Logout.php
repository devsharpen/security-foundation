<?php

declare(strict_types = 1);

namespace Devsharpen\Security\Foundation\Http\Logout;

use Devsharpen\Security\Foundation\Core\Authentication\Token\Tokenable;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

interface Logout
{
    public function logout(Request $request, Response $response, Tokenable $token);
}