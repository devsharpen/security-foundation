<?php

declare(strict_types = 1);

namespace Security\Foundation\Http\Authorization;

use Devsharpen\Security\Foundation\Core\Exception\AuthorizationException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

interface AuthorizationDenied
{

    public function handle(Request $request, AuthorizationException $accessDeniedException): Response;
}