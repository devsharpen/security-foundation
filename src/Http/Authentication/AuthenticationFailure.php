<?php

declare(strict_types = 1);

namespace Security\Foundation\Http\Authentication;

use Devsharpen\Security\Foundation\Core\Exception\AuthenticationException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

interface AuthenticationFailure
{
    public function onAuthenticationSuccess(Request $request, AuthenticationException $exception): Response;
}