<?php

declare(strict_types = 1);

namespace Security\Foundation\Http\Authentication;

use Devsharpen\Security\Foundation\Core\Authentication\Token\Tokenable;
use Symfony\Component\HttpFoundation\Response;

interface AuthenticationSuccess
{
    public function onAuthenticationSuccess(Tokenable $token): Response;
}